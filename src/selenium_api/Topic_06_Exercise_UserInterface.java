package selenium_api;

import org.testng.annotations.Test;


import bsh.commands.dir;

import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_06_Exercise_UserInterface {

	WebDriver driver;
	Actions action;
	Alert alert;

	@BeforeClass
	public void beforeClass() {
		// driver = new FirefoxDriver();
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		driver = new ChromeDriver();
		action = new Actions(driver);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test(enabled = false)
	public void TC_01_moveMouseToElement() {
		driver.get("http://www.myntra.com/");
		WebElement accountIcon = driver.findElement(By.xpath("//div[@class='desktop-userIconsContainer']"));
		action.moveToElement(accountIcon).perform();
		WebElement buttonLogin = driver.findElement(By.xpath("//a[text()='login']"));
		buttonLogin.click();
		Assert.assertTrue(driver.findElement(By.xpath("//p[(@class='login-title' and text()='Login to Myntra')]")).isDisplayed());
	}

	@Test(enabled = false)
	private void TC_02_clickAndHoldSelectMultipleItem1() {
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		List<WebElement> listItems = driver.findElements(By.xpath("//ol[@id='selectable']/li"));
		// Click and Hold 1--> 4
		action.clickAndHold(listItems.get(0)).moveToElement(listItems.get(3)).release().perform();
		List<WebElement> numberSelected = driver.findElements(By.xpath("//ol[@id='selectable']/li[@class='ui-state-default ui-selectee ui-selected']"));
		Assert.assertEquals(numberSelected.size(), 4);
	}

	@Test(enabled = false)
	public void TC_02_clickAndHoldSelectMultipleItem2() {
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		List<WebElement> listItems = driver.findElements(By.xpath("//ol[@id='selectable']/li"));
		// Click and Hold (1-3-5-7)
		action.keyDown(Keys.CONTROL).build().perform();
		listItems.get(0).click();
		listItems.get(2).click();
		listItems.get(4).click();
		listItems.get(6).click();
		action.keyUp(Keys.CONTROL).build().perform();
		List<WebElement> numberSelected = driver.findElements(By.xpath("//ol[@id='selectable']/li[@class='ui-state-default ui-selectee ui-selected']"));
		Assert.assertEquals(numberSelected.size(), 4);
	}

	@Test(enabled = false)
	public void TC_03_doubleClick() {
		driver.get("http://www.seleniumlearn.com/double-click");
		WebElement buttonClick = driver.findElement(By.xpath("//button[text()='Double-Click Me!']"));
		action.doubleClick(buttonClick).perform();
		Alert alert = driver.switchTo().alert();
		Assert.assertEquals(alert.getText(), "The Button was double-clicked.");
		alert.accept();
	}

	@Test(enabled = false)
	public void TC_04_rightClick() throws Exception {
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		WebElement rightClickButton = driver.findElement(By.xpath("//span[text()='right click me']"));
		action.contextClick(rightClickButton).perform();
		Thread.sleep(2000);
		// verify quit displayed and not hover mouse
		WebElement quitButton = driver.findElement(By.xpath("//li[contains(@class, 'context-menu-icon-quit') and not(contains(@class, 'context-menu-hover'))]"));
		Assert.assertTrue(quitButton.isDisplayed());
		// hover to quit
		action.moveToElement(quitButton).perform();
		// verify quit visible and hover mouse
		Assert.assertTrue(driver.findElement(By.xpath("//li[contains(@class, 'context-menu-item') and contains(@class, 'context-menu-icon-quit') and (contains(@class, 'context-menu-hover'))]")).isDisplayed());
		action.click(quitButton).perform();
		// Check quit click and aler displayed
		Thread.sleep(2000);
		alert = driver.switchTo().alert();
		Assert.assertEquals(alert.getText(), "clicked: quit");
		alert.accept();
	}

	@Test(enabled = false)
	public void TC_05_dragAndDrop() throws Exception {
		driver.get("http://demos.telerik.com/kendo-ui/dragdrop/angular");
		WebElement dragElement = driver.findElement(By.xpath("//div[@id='draggable']"));
		WebElement dropElement = driver.findElement(By.xpath("//div[@id='droptarget']"));
		action.dragAndDrop(dragElement, dropElement).perform();
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='droptarget' and contains(text(), 'You did great!')]")).isDisplayed());

	}

	@Test
	public void TC_06_enterKey() {
		driver.get("https://www.myntra.com/login");
		driver.findElement(By.xpath("//input[@placeholder ='Your Email Address']")).sendKeys("automation@gmail.com");
		WebElement login = driver.findElement(By.xpath("//button[text()='Log in']"));
		action.sendKeys(login, Keys.ENTER).perform();
		Assert.assertTrue(driver.findElement(By.xpath("//p[@class='login-error-message' and text()='Password must be at least 6 characters']")).isDisplayed());
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
