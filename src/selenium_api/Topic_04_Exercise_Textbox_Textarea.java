package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;



import java.util.Random;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_04_Exercise_Textbox_Textarea {
	WebDriver driver;
	String customID;
	String customerName, gender, dateOfBirth, address, city, state, pin, mobileNumber, email, password;
	String editAddress, editCity, editState, editPin, editMobileNumber, editEmail;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();

		// info Create Customer
		customerName = "Huyen Pham";
		gender = "male";
		dateOfBirth = "1992-10-10";
		address = "123 Hong Hai";
		city = "Ha Nam";
		state = "Phu Ly";
		pin = "123456";
		mobileNumber = "0923123456";
		email = "automation" + random() + "@gmail.com";
		password = "123456";

		// info Edit Customer
		editAddress = "321 Bach Dang";
		editCity = "Ha Noi";
		editState = "Tu Liem";
		editPin = "345678";
		editMobileNumber = "0921234213";
		editEmail = "automationedit" + random() + "@gmail.com";

	}

	@Test
	public void TC_03_CreateAccount() {
		driver.get("http://demo.guru99.com/v4");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys("mngr155533");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("aqAtAda");
		driver.findElement(By.xpath("//input[@type='submit']")).click();

		Assert.assertTrue(driver.findElement(By.xpath("//marquee[text()=\"Welcome To Manager's Page of Guru99 Bank\"]")).isDisplayed());
		// Click link New Customer
		driver.findElement(By.xpath("//a[text()='New Customer']")).click();

		// Create New Customer
		driver.findElement(By.xpath("//input[@name='name']")).sendKeys(customerName);
		driver.findElement(By.xpath("//input[@value='m']")).click();
		driver.findElement(By.xpath("//input[@id='dob']")).sendKeys(dateOfBirth);
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys(address);
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(city);
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys(state);
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys(pin);
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys(mobileNumber);
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
		driver.findElement(By.xpath("//input[@type='submit']")).click();

		// verify create customer page updated success with message
		Assert.assertTrue(driver.findElement(By.xpath("//p[text()='Customer Registered Successfully!!!']")).isDisplayed());

		// Verify Create Customer Success with above Information
		customID = driver.findElement(By.xpath("//td[text()='Customer ID']/following-sibling::td")).getText();
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Customer Name']/following-sibling::td")).getText(), customerName);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Gender']/following-sibling::td")).getText(), gender);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Birthdate']/following-sibling::td")).getText(), dateOfBirth);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Address']/following-sibling::td")).getText(), address);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='City']/following-sibling::td")).getText(), city);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='State']/following-sibling::td")).getText(), state);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Pin']/following-sibling::td")).getText(), pin);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Mobile No.']/following-sibling::td")).getText(), mobileNumber);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Email']/following-sibling::td")).getText(), email);

		System.out.println("Customer ID: " + customID);
	}

	@Test(enabled = false)
	public void TC_03_EditAccount() {
		// TODO Auto-generated method stub
		driver.findElement(By.xpath("//a[contains(.,'Edit Customer')]")).click();

		// input customID edit
		driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys(customID);
		driver.findElement(By.xpath("//input[@type='submit']")).click();

		// display screen edit customer
		Assert.assertTrue(driver.findElement(By.xpath("//p[text()='Edit Customer']")).isDisplayed());

		// verify text Customer Name and Address

		Assert.assertEquals(driver.findElement(By.xpath("//input[@name='name']")).getAttribute("value"), customerName);
		Assert.assertEquals(driver.findElement(By.xpath("//textarea[@name='addr']")).getText(), address);

		// Edit Customer
		driver.findElement(By.xpath("//textarea[@name ='addr']")).clear();
		driver.findElement(By.xpath("//input[@name='city']")).clear();
		driver.findElement(By.xpath("//input[@name='state']")).clear();
		driver.findElement(By.xpath("//input[@name='pinno']")).clear();
		driver.findElement(By.xpath("//input[@name='telephoneno']")).clear();
		driver.findElement(By.xpath("//input[@name='emailid']")).clear();

		driver.findElement(By.xpath("//textarea[@name ='addr']")).sendKeys(editAddress);
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(editCity);
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys(editState);
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys(editPin);
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys(editMobileNumber);
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys(editEmail);

		driver.findElement(By.xpath("//input[@name='sub']")).click();

		// verify edit customer page updated success with message
		Assert.assertTrue(driver.findElement(By.xpath("//p[text()='Customer details updated Successfully!!!']")).isDisplayed());

		// verify edit customer update success with above information
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Address']/following-sibling::td")).getText(), editAddress);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='City']/following-sibling::td")).getText(), editCity);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='State']/following-sibling::td")).getText(), editState);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Pin']/following-sibling::td")).getText(), editPin);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Mobile No.']/following-sibling::td")).getText(), editMobileNumber);
		Assert.assertEquals(driver.findElement(By.xpath("//td[text()='Email']/following-sibling::td")).getText(), editEmail);

	}

	public int random() {
		Random random = new Random();
		int number = random.nextInt(9999);
		return number;

	}

	@AfterClass

	public void afterClass() {
		driver.quit();
	}

}
