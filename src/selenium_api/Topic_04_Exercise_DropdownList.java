package selenium_api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_04_Exercise_DropdownList {

	WebDriver driver;
	WebDriverWait wait;
	Select select;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		// System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		// driver = new ChromeDriver();

		wait = new WebDriverWait(driver, 30);
	}

	@Test(enabled = false)
	public void TC_01_HTMLDropdownList() {
		driver.get("https://daominhdam.github.io/basic-form/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		select = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		// Step 02. Kiểm tra dropdown Job Role 01 không hỗ trợ thuộc tính multi select
		Assert.assertFalse(select.isMultiple());
		// Step 03 - Chọn giá trị Automation Tester trong dropdown bằng phương thức
		// selectVisible
		select.selectByVisibleText("Automation Tester");
		// Step 04 - Kiểm tra giá trị đã được chọn thành công
		getFirstSeletct("Automation Tester");

		// Step 05 - Chọn giá trị Manual Tester trong dropdown bằng phương thức
		// selectValue
		// Step 06 - Kiểm tra giá trị đã được chọn thành công
		select.selectByValue("manual");
		getFirstSeletct("Manual Tester");

		// Step 07 - Chọn giá trị Mobile Tester trong dropdown bằng phương thức
		// selectIndex
		// Step 08 - Kiểm tra giá trị đã được chọn thành công
		select.selectByIndex(3);
		getFirstSeletct("Mobile Tester");
		// Step 09 - Kiểm tra dropdown có đủ 5 giá trị
		Assert.assertEquals(5, select.getOptions().size());
	}

	public void getFirstSeletct(String textoption) {
		Assert.assertEquals(select.getFirstSelectedOption().getText(), textoption);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(enabled = false)
	public void TC_02_CustomDropDown_URL1() throws Exception {
		/*
		 * Dropdown in URL:http://jqueryui.com/resources/demos/selectmenu/default.html
		 */
		driver.get("http://jqueryui.com/resources/demos/selectmenu/default.html");
		selectItemInCustomerDropdown("//span[@id='number-button']", "//ul[@id='number-menu']/li[@class='ui-menu-item']/div", "19");
		// kiem tra gia tri đã được chọn thành công
		Assert.assertTrue(driver.findElement(By.xpath("//span[@id='number-button']/span[@class='ui-selectmenu-text' and text()='19']")).isDisplayed());
		// Dropdown in URL:http://jqueryui.com/resources/demos/selectmenu/default.html
	}

	@Test(enabled = false)
	public void TC_02_CustomDropDown_URL2() throws Exception {
		/* Dropdown in URL:https://material.angular.io/components/select/examples */
		driver.get("https://material.angular.io/components/select/examples");
		selectItemInCustomerDropdown("//mat-select[@placeholder='State']", "//mat-option/span", "Alaska");
		// //kiem tra gia tri đã được chọn thành công
		Assert.assertTrue(driver.findElement(By.xpath("//mat-select[@placeholder='State']//span[text()='Alaska']")).isDisplayed());

	}

	@Test(enabled = false)
	public void TC_02_CustomDropDown_URL3() throws Exception {
		/*
		 * Dropdown in URL:https://demos.telerik.com/kendo-ui/dropdownlist/index
		 */
		driver.get("https://demos.telerik.com/kendo-ui/dropdownlist/index ");
		selectItemInCustomerDropdown("//div[@id='cap-view']/span[1]/span", "//div[@class='k-list-scroller']/ul[@id='color_listbox']/li", "Orange");
		// kiem tra gia tri đã được chọn thành công
		Assert.assertTrue(driver.findElement(By.xpath("//div[@id='cap-view']/span[1]/span/span[contains(.,'Orange')]")).isDisplayed());
	}

	@Test(enabled = false)
	public void TC_02_CustomDropDown_URL4() throws Exception {
		/*
		 * Dropdown in URL:https://mikerodham.github.io/vue-dropdowns/
		 */
		driver.get("https://mikerodham.github.io/vue-dropdowns/");
		selectItemInCustomerDropdown("//li[@class='dropdown-toggle']", "//ul[@class='dropdown-menu']/li/a", "Third Option");
		// kiem tra gia tri đã được chọn thành công
		Assert.assertTrue(driver.findElement(By.xpath("//li[@class='dropdown-toggle' and contains(.,'Third Option')]")).isDisplayed());

	}

	@Test(enabled = false)
	public void TC_02_CustomDropDown_URL5() throws Exception {
		/*
		 * Dropdown in URL:http://indrimuska.github.io/jquery-editable-select/
		 */
		String expectValue1 = "Citroen";
		driver.get("http://indrimuska.github.io/jquery-editable-select/");
		driver.findElement(By.xpath("//div[@id='default-place']/input")).sendKeys("C");
		Thread.sleep(1000);
		List<WebElement> allItemsDropdown = driver.findElements(By.xpath("//div[@id='default-place']/ul/li[contains(@style,'display: block') or contains(@class,'es-visible')]"));
		int itemNumber = allItemsDropdown.size();
		System.out.println(itemNumber);

		// 3. duyệt qua từng item trong list element và get text
		for (int i = 0; i < itemNumber; i++) {
			System.out.println(allItemsDropdown.get(i).getText());
			if (allItemsDropdown.get(i).getText().equals(expectValue1)) {
				allItemsDropdown.get(i).click();
				break;
			}
		}
		// kiem tra gia tri đã được chọn thành công
		String inputText = driver.findElement(By.xpath("//div[@id='default-place']/input")).getAttribute("value");
		Assert.assertEquals(inputText, expectValue1);
	}

	@Test(enabled = false)
	public void TC_02_CustomDropDown_URL6() throws Exception {
		/*
		 * Dropdown in URL(advanced):http://wenzhixin.net.cn/p/multiple-select/docs/
		 */
		driver.get("http://wenzhixin.net.cn/p/multiple-select/docs/");

		String[] monthArray = { "February", "January", "March", "April" };
		selectItemsDropdownAdvance("//p[contains(.,'and turns it into:')]/following-sibling:: p[@id='e1_t']/div/button", "//p[@id='e1_t']//ul/li//input/following-sibling:: span", monthArray);

		/* _________ kiem tra gia tri đã được chọn thành công_________________ */
		// 1. Verify giá trị hiển thị
		String verifyText = "4 of 12 selected";
		Assert.assertEquals(driver.findElement(By.xpath("//p[@id='e1_t']/div/button/span")).getText(), verifyText);
		// 2. Verify các giá trị trong list được check
		VerifyItemsSelected("//p[@id='e1_t']//ul/li[@class='selected']//input/following-sibling:: span", monthArray);

	}

	@Test
	public void TC_02_CustomDropDown_URL7() throws Exception {
		/*
		 * Dropdown in URL(advanced):https://semantic-ui.com/modules/dropdown.html
		 */
		String[] expectArray1 = { "Graphic Design", "Javascript", "Rails" };
		String[] expectArray2 = { "Antigua", "Benin", "Vietnam" };
		driver.get("https://semantic-ui.com/modules/dropdown.html#/examples");

		// Chọn giá trị trong 2 dropdown
		selectItemsDropdownAdvance("//div[contains(.,'Selection')]/following-sibling::div[@class='ui fluid normal dropdown selection multiple']", "//div[@class='menu transition visible']/div", expectArray1);
		selectItemsDropdownAdvance("//div[contains(.,'Search Selection')]/following-sibling::div[@class='ui fluid multiple search normal selection dropdown']", "//div[@class='menu transition visible']/div", expectArray2);

		/* _________ kiem tra list gia tri đã được chọn thành công_________________ */
		// List 1
		VerifyItemsSelected("//div[@class='ui fluid normal dropdown selection multiple upward']/a", expectArray1);
		// List 2
		VerifyItemsSelected("//div[@class='ui fluid multiple search normal selection dropdown active visible']/a", expectArray2);

	}

	public void selectItemInCustomerDropdown(String parentLocator, String allItemLocator, String expectValue) throws Exception {

		// 1.Scoll and click to open dropdown
		WebElement parentDropdown = driver.findElement(By.xpath(parentLocator));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", parentDropdown);
		Thread.sleep(1000);
		// wait.until(ExpectedConditions.visibilityOf(parentDropdown));

		// driver.findElement(By.xpath(parentLocator)).click();
		List<WebElement> allItemsDropdown = driver.findElements(By.xpath(allItemLocator));
		int itemNumber = allItemsDropdown.size();
		// 2. wait all items visible
		wait.until(ExpectedConditions.visibilityOfAllElements(allItemsDropdown));

		// 3. duyệt qua từng item trong list element và get text
		for (int i = 0; i < itemNumber; i++) {
			System.out.println(allItemsDropdown.get(i).getText());
			if (allItemsDropdown.get(i).getText().equals(expectValue)) {
				allItemsDropdown.get(i).click();
				break;
			}
		}
	}

	public void selectItemsDropdownAdvance(String parentLocator, String allItemLocator, String[] expectValues) throws Exception {

		// 1.Scoll and click to open dropdown
		WebElement parentDropdown = driver.findElement(By.xpath(parentLocator));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", parentDropdown);
		Thread.sleep(1000);

		List<WebElement> allItemsDropdown = driver.findElements(By.xpath(allItemLocator));
		// 2. wait all items visible
		wait.until(ExpectedConditions.visibilityOfAllElements(allItemsDropdown));

		// 3. duyệt qua từng item trong list element và get text
		for (int i = 0; i < allItemsDropdown.size(); i++) {
			for (int j = 0; j < expectValues.length; j++) {
				if (allItemsDropdown.get(i).getText().equals(expectValues[j])) {
					allItemsDropdown.get(i).click();
				}
			}
		}
	}

	public void VerifyItemsSelected(String itemLocatorSelected, String[] expectArray) {

		List<WebElement> itemsSelected = driver.findElements(By.xpath(itemLocatorSelected));
		System.out.println("So phan tu : " + itemsSelected.size());
		System.out.println("-----------RESULT ARRAY----------");
		String[] resultArray = new String[itemsSelected.size()];
		for (int i = 0; i < itemsSelected.size(); i++) {
			resultArray[i] = itemsSelected.get(i).getText();
			System.out.println(resultArray[i]);
		}
		Arrays.sort(expectArray);
		Arrays.sort(resultArray);
		Assert.assertEquals(expectArray, resultArray);

	}

	@AfterClass
	public void afterClass() {
		 driver.quit();
	}

}
