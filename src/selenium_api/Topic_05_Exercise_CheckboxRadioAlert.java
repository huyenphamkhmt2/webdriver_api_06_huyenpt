package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_05_Exercise_CheckboxRadioAlert {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test(enabled = false)
	public void TestScript_01() throws Exception {

		driver.get("http://live.guru99.com/");
		WebElement myAccount = driver.findElement(By.xpath("//li[@class='first']/a[contains(.,'My Account')]"));
		clickElementByJavascript(myAccount);
		Thread.sleep(3000);
		String pageURLcurrent1 = "http://live.guru99.com/index.php/customer/account/login/";
		Assert.assertEquals(driver.getCurrentUrl(), pageURLcurrent1);
		WebElement createAccount = driver.findElement(By.xpath("//span[contains(.,'Create an Account')]"));
		clickElementByJavascript(createAccount);
		String pageURLcurrent2 = "http://live.guru99.com/index.php/customer/account/create/";
		Assert.assertEquals(driver.getCurrentUrl(), pageURLcurrent2);
	}

	@Test(enabled = false)
	public void TestScript_02() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		WebElement checkboxSelected = driver.findElement(By.xpath("//label[text()='Dual-zone air conditioning']/preceding-sibling:: input"));
		clickElementByJavascript(checkboxSelected);
		Assert.assertTrue(checkboxSelected.isSelected());
		if (checkboxSelected.isSelected()) {
			clickElementByJavascript(checkboxSelected);
		}
		Assert.assertFalse(checkboxSelected.isSelected());
	}

	@Test(enabled = false)
	public void TestScript_03() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/radios");
		WebElement radioSelected = driver.findElement(By.xpath("//label[contains(.,'2.0 Petrol, 147kW')]/preceding-sibling:: input"));
		clickElementByJavascript(radioSelected);
		Assert.assertTrue(radioSelected.isSelected());
		if (!radioSelected.isSelected()) {
			clickElementByJavascript(radioSelected);
		}

	}

	@Test(enabled = false)
	public void TestScript_04() {
		driver.get("http://daominhdam.890m.com/");
		driver.findElement(By.xpath("//button[text()='Click for JS Alert']")).click();
		Alert alert = driver.switchTo().alert();
		String textonAlert = alert.getText();
		Assert.assertEquals(textonAlert, "I am a JS Alert");
		alert.accept();
		String verifyText = driver.findElement(By.xpath("//p[@id='result']")).getText();
		Assert.assertEquals(verifyText, "You clicked an alert successfully");

	}

	@Test(enabled = false)
	public void TestScript_05() {
		driver.get("http://daominhdam.890m.com/");
		driver.findElement(By.xpath("//button[contains(.,'Click for JS Confirm')]")).click();
		Alert alert = driver.switchTo().alert();
		String textonAlert = alert.getText();
		Assert.assertEquals(textonAlert, "I am a JS Confirm");
		alert.dismiss();
		Assert.assertTrue(driver.findElement(By.xpath("//p[@id='result' and text()='You clicked: Cancel']")).isDisplayed());

	}

	@Test(enabled = false)
	public void TestScript_06() {
		driver.get("http://daominhdam.890m.com/");
		driver.findElement(By.xpath("//button[contains(.,'Click for JS Prompt')]")).click();
		Alert alert = driver.switchTo().alert();
		String textonAlert = alert.getText();
		Assert.assertEquals(textonAlert, "I am a JS prompt");
		alert.sendKeys("phamthihuyen");
		alert.accept();
		Assert.assertTrue(driver.findElement(By.xpath("//p[@id='result' and text()='You entered: phamthihuyen']")).isDisplayed());
	}

	@Test(enabled = false)
	public void TestScript_07() {
		driver.get("http://admin:admin@the-internet.herokuapp.com/basic_auth");
		Assert.assertTrue(driver.findElement(By.xpath("//p[contains(.,'Congratulations! You must have the proper credentials.')]")).isDisplayed());
	}

	public void clickElementByJavascript(WebElement element) {
		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("arguments[0].click()", element);
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
