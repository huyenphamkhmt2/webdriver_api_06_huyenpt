package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_07_Exercise_WindowPopup {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test(enabled = false)
	public void TC_02_Popup2Windows() {
		driver.get("https://daominhdam.github.io/basic-form/index.html");
		String parentWindow = driver.getWindowHandle();
		driver.findElement(By.xpath("//label[text()='Opening a new window:']/following-sibling:: a[text()='Click Here']")).click();
		switchToChildWindow(parentWindow);
		pageCurrentUrl("Google");
		driver.close();
		driver.switchTo().window(parentWindow);
		pageCurrentUrl("SELENIUM WEBDRIVER FORM DEMO");
	}

	@Test
	public void TC_03_PopupWithMultiWindows() throws Exception {

		driver.get("http://www.hdfcbank.com/");
		String parentWindow = driver.getWindowHandle();
		/* __________STEP 02_________________________ */
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> notificationIframe = driver.findElements(By.xpath("//iframe[@id='vizury-notification-template']"));
		System.out.println("List element size = " + notificationIframe.size());
		if (notificationIframe.size() > 0) {
			driver.switchTo().frame(notificationIframe.get(0));
			System.out.println("Click close icon");
			WebElement closeIcon = driver.findElement(By.xpath("//div[@id='div-close']"));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].click()", closeIcon);
			Assert.assertFalse(closeIcon.isDisplayed());
		}
		driver.switchTo().defaultContent();
		driver.findElement(By.xpath("//a[text()='Agri']")).click();
		// switch to other tab
		switchToWindowByTitle("HDFC Bank Kisan Dhan Vikas e-Kendra");
		driver.findElement(By.xpath("//p[text()='Account Details']")).click();
		switchToWindowByTitle("Welcome to HDFC Bank NetBanking");
		WebElement footerframe = driver.findElement(By.xpath("//frame[@src='footer.html']"));
		driver.switchTo().frame(footerframe);
		driver.findElement(By.xpath("//p[@class='footer']/a[text()='Privacy Policy']")).click();
		switchToWindowByTitle("HDFC Bank - Leading Bank in India, Banking Services, Private Banking, Personal Loan, Car Loan");
		driver.findElement(By.xpath("//a[text()='CSR']")).click();
		closeAllWithoutParentWindows(parentWindow);
		String pageCurrentURL = driver.getCurrentUrl();
		Assert.assertEquals(pageCurrentURL, "http://www.hdfcbank.com");
	}

	// Switch to child Windows (only 2 windows)
	public void switchToChildWindow(String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(parent)) {
				driver.switchTo().window(runWindow);
				break;
			}
		}
	}

	// Switch to child Windows (greater than 2 windows and title of the pages are
	// unique)
	public void switchToWindowByTitle(String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			driver.switchTo().window(runWindows);
			String currentWin = driver.getTitle();
			if (currentWin.equals(title)) {
				break;
			}
		}
	}
	// Close all windows without parent window

	public boolean closeAllWithoutParentWindows(String parentWindow) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			if (!runWindows.equals(parentWindow)) {
				driver.switchTo().window(runWindows);
				driver.close();
			}
		}
		driver.switchTo().window(parentWindow);
		if (driver.getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}

	public void pageCurrentUrl(String titleText) {
		String titleCurentURL = driver.getTitle();
		Assert.assertEquals(titleCurentURL, titleText);
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
