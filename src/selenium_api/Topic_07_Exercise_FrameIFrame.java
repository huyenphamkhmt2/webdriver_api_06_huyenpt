package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_07_Exercise_FrameIFrame {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	public void TestScript_01() {
		driver.get("http://www.hdfcbank.com/");
		/* __________STEP 02_________________________ */
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> notificationIframe = driver.findElements(By.xpath("//iframe[@id='vizury-notification-template']"));
		System.out.println("List element size = " + notificationIframe.size());
		if (notificationIframe.size() > 0) {
			driver.switchTo().frame(notificationIframe.get(0));
			System.out.println("Click close icon");
		//	driver.findElement(By.xpath("//div[@id='div-close']")).click();
			WebElement closeIcon = driver.findElement(By.xpath("//div[@id='div-close']"));
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].click()", closeIcon);
			Assert.assertFalse(closeIcon.isDisplayed());
		}
		driver.switchTo().defaultContent();
		/* ________Step 03_________ */
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("STEP 03");
		WebElement flipBannerWrap = driver.findElement(By.xpath("//div[@class='flipBannerWrap']//iframe"));
		driver.switchTo().frame(flipBannerWrap);
		Assert.assertTrue(driver.findElement(By.xpath("//span[@id='messageText' and text()='What are you looking for?']")).isDisplayed());
		driver.switchTo().defaultContent();
		/* ____________STEP 04___________ */
		System.out.println("STEP 04");
		WebElement slidingBanner = driver.findElement(By.xpath("//div[@class='slidingbanners']//iframe"));
		driver.switchTo().frame(slidingBanner);
		List<WebElement> bannerImages = driver.findElements(By.xpath("//div[@id='productcontainer']//img[@class='bannerimage']"));
		Assert.assertEquals(bannerImages.size(), 6);
		driver.switchTo().defaultContent();
		/* ____________STEP 05___________ */
		System.out.println("STEP 05");
		
		List<WebElement> flipImages = driver.findElements(By.xpath("//div[@class='flipBanner']//img[@class='front icon']"));
		Assert.assertEquals(flipImages.size(),8);
		System.out.println("CHECK BANNER IS DISPLAYED");
		for(int i =0; i< flipImages.size(); i++) {
		Assert.assertTrue(flipImages.get(i).isDisplayed());
		}
	}

	@AfterClass
	public void afterClass() {
		//driver.quit();
	}

}
