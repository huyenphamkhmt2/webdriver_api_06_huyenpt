package selenium_api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.PauseAction;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_02_Exercise {

	/* _______________Topic 02_exercise____________________ */

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
	}

	@Test
	public void TC_01_VerifyUrlAndTitle() {
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		String homePageTitle = driver.getTitle();
		Assert.assertEquals(homePageTitle, "Home page");
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//span[text()='Create an Account']")).click();
		driver.navigate().back();
		String loginPageUrl = driver.getCurrentUrl();
		Assert.assertEquals(loginPageUrl, "http://live.guru99.com/index.php/customer/account/login/");
		driver.navigate().forward();
		String registerPagUrl = driver.getCurrentUrl();
		Assert.assertEquals(registerPagUrl, "http://live.guru99.com/index.php/customer/account/create/");

	}

	@Test(enabled = false)
	public void TC_02_LoginWithEmailAndPasswordEmpty() {
		// TODO Auto-generated method stub
		driver.get("http://live.guru99.com/");
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		String emailErrorText = driver.findElement(By.xpath("//div[@id='advice-required-entry-email']")).getText();
		Assert.assertEquals(emailErrorText, "This is a required field.");
		String passwordErrorText = driver.findElement(By.xpath("//div[@id='advice-required-entry-pass']")).getText();
		Assert.assertEquals(passwordErrorText, "This is a required field.");
	}

	@Test(enabled = false)
	public void TC_03_LoginWithEmailInvalid() {
		driver.get("http://live.guru99.com/");
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("123434234@12312.123123");
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		String emailErrorText = driver.findElement(By.xpath("//div[@id='advice-validate-email-email']")).getText();
		Assert.assertEquals(emailErrorText, "Please enter a valid email address. For example johndoe@domain.com.");
	}

	@Test(enabled = false)
	public void TC_04_LoginWithPasswordIncorrect() {
		// TODO Auto-generated method stub
		driver.get("http://live.guru99.com/");
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("automation@gmail.com");
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("123");
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		String passwordErrorText = driver.findElement(By.xpath("//div[@id='advice-validate-password-pass']")).getText();
		Assert.assertEquals(passwordErrorText, "Please enter 6 or more characters without leading or trailing spaces.");

	}

	@Test
	public void TC_05_CreateAnAccount() throws Exception {
		// Register Account
		driver.get("http://live.guru99.com/");
		driver.findElement(By.xpath("//div[@class='footer']//a[@title='My Account']")).click();
		driver.findElement(By.xpath("//span[text()='Create an Account']")).click();
		driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("AUTOMATION");
		driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("TESTING");
		driver.findElement(By.xpath("//input[@id='email_address']"))
				.sendKeys("automation" + random() + "@gmail.com");
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//input[@id='confirmation']")).sendKeys("123456");
		driver.findElement(By.xpath("//button[@title='Register']")).click();
		String verifyText = driver.findElement(By.xpath("//li[@class='success-msg']//span")).getText();
		Assert.assertEquals(verifyText, "Thank you for registering with Main Website Store.");
		// Logout Account
		driver.findElement(By.xpath("//div[@class='account-cart-wrapper']//span[text()='Account']")).click();
		driver.findElement(By.xpath("//div[@id='header-account']//a[@title='Log Out']")).click();
		Thread.sleep(7000);
		String homePageTitle1 = driver.getTitle();
		Assert.assertEquals(homePageTitle1, "Home page");

	}
	public int random() {
		Random random = new Random();
		int number = random.nextInt(9999);
		return number;
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
