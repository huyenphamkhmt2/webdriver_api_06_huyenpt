package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_09_Exercise_UploadFile {

	WebDriver driver;
	String projectFolder = System.getProperty("user.dir");
	String fileName01 = "hoacuc1.jpg";
	String fileName02 = "hoacuc2.jpg";
	String fileName03 = "hoacuc3.jpg";

	String filePath01 = projectFolder + "\\upload\\" + fileName01;
	String filePath02 = projectFolder + "\\upload\\" + fileName02;
	String filePath03 = projectFolder + "\\upload\\" + fileName03;

	@BeforeClass
	public void beforeClass() {
		System.setProperty("webdriver.gecko.driver", "driver/geckodriver.exe");
		driver = new FirefoxDriver();
		// System.setProperty("webdriver.ie.driver", "driver/IEDriverServer.exe");
		// System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		// driver = new InternetExplorerDriver();
		// driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test(enabled = false)
	public void TC_01_uploadFileBySendkeys() throws Exception {
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		// tai 3 file
		String file[] = { filePath01, filePath02, filePath03 };
		for (int i = 0; i < file.length; i++) {
			WebElement upload = findElement("//input[@type='file']");
			upload.sendKeys(file[i]);
			Thread.sleep(1000);
		}
		// verify 3 file duoc upload thanh cong
		Assert.assertTrue(findElement("//p[text()= '" + fileName01 + "']").isDisplayed());
		Assert.assertTrue(findElement("//p[text()='" + fileName02 + "']").isDisplayed());
		Assert.assertTrue(findElement("//p[text()='" + fileName03 + "']").isDisplayed());
		// Click Start button để upload cho cả 3 file
		List<WebElement> startButton = driver.findElements(By.xpath("//table//button[@class = 'btn btn-primary start']"));
		for (WebElement start : startButton) {
			start.click();
			Thread.sleep(1000);
		}
		// Verify thanh cong 3 file duoc upload
		Assert.assertTrue(findElement("//p[@class='name']/a[text()='" + fileName01 + "']").isDisplayed());
		Assert.assertTrue(findElement("//p[@class='name']/a[text()='" + fileName02 + "']").isDisplayed());
		Assert.assertTrue(findElement("//p[@class='name']/a[text()='" + fileName03 + "']").isDisplayed());
	}

	@Test
	public void TC_02_uploadMultiFile() throws Exception {
		driver.get("http://blueimp.github.com/jQuery-File-Upload/");
		Thread.sleep(5000);
		WebElement upload = findElement("//input[@type='file']");
		upload.sendKeys(filePath01 + "\n" + filePath02 + "\n" + filePath03);
		Thread.sleep(5000);
		Assert.assertTrue(findElement("//p[text()= '" + fileName01 + "']").isDisplayed());
		Assert.assertTrue(findElement("//p[text()='" + fileName02 + "']").isDisplayed());
		Assert.assertTrue(findElement("//p[text()='" + fileName03 + "']").isDisplayed());
		// Click Start button để upload cho cả 3 file
		List<WebElement> startButton = driver.findElements(By.xpath("//table//button[@class = 'btn btn-primary start']"));
		for (WebElement start : startButton) {
			start.click();
			Thread.sleep(1000);
		}
		// Verify thanh cong 3 file duoc upload
		Assert.assertTrue(findElement("//p[@class='name']/a[text()='" + fileName01 + "']").isDisplayed());
		Assert.assertTrue(findElement("//p[@class='name']/a[text()='" + fileName02 + "']").isDisplayed());
		Assert.assertTrue(findElement("//p[@class='name']/a[text()='" + fileName03 + "']").isDisplayed());
	}

	public WebElement findElement(String elementText) {
		return driver.findElement(By.xpath(elementText));
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
// Muốn chạy được thì update firefox mới nhất với geckodriver. Chrome, IE driver
// mới nhất và bản
// selenium-server-standalone mới nhất 3.141.0
