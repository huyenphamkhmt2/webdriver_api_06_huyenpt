package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Topic_03_Exercise {

	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
	}

	@Test(enabled=false)
	public void TC_01_VerifyElementIsDisplayed() {

	driver.get("http://daominhdam.890m.com/");
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	WebElement emailTextbox = driver.findElement(By.xpath("//input[@id='mail']"));
	if (emailTextbox.isDisplayed()) {
		emailTextbox.sendKeys("Automation Testing");
	}
	WebElement under18Radio = driver.findElement(By.xpath("//input[@id='under_18']"));
	if (under18Radio.isDisplayed()) {
		under18Radio.click();;
	}
	WebElement educationTextbox = driver.findElement(By.xpath("//textarea[@id='edu']"));
	if (educationTextbox.isDisplayed()) {
		educationTextbox.sendKeys("Automation Testing");
	}
	
	}
	
	@Test(enabled=false)
	public void TC_02_VerifyElementIsEnabled() {
		// TODO Auto-generated method stub
		driver.get("http://daominhdam.890m.com/");
		
		//element enabled
		WebElement emailTextbox = driver.findElement(By.xpath("//input[@id='mail']"));
		WebElement under18Radio = driver.findElement(By.xpath("//input[@id='under_18']"));
		WebElement educationTextbox = driver.findElement(By.xpath("//textarea[@id='edu']"));
		WebElement jobRole1Select = driver.findElement(By.xpath("//select[@id='job1']"));
		WebElement interestsDevelopment = driver.findElement(By.xpath("//input[@id='development']"));
		WebElement Slider1 = driver.findElement(By.xpath("//input[@id='slider-1']"));
		WebElement buttonIsEnabled = driver.findElement(By.xpath("//button[@id='button-enabled']"));

		
		
		isElementEnabled(emailTextbox);
		isElementEnabled(under18Radio);
		isElementEnabled(educationTextbox);
		isElementEnabled(jobRole1Select);
		isElementEnabled(interestsDevelopment);
		isElementEnabled(Slider1);
		isElementEnabled(buttonIsEnabled);
		
		//element disabled
		WebElement passTextbox = driver.findElement(By.xpath("//input[@id='password']"));
		WebElement ageRadioIsDisabled = driver.findElement(By.xpath("//input[@id='radio-disabled'][@name='user_age']"));
		WebElement biographyTextarea = driver.findElement(By.xpath("//textarea [@id='bio']"));
		WebElement jobRole2Select = driver.findElement(By.xpath("//select[@id='job2']"));
		WebElement interestsCheckboxIsDisabled = driver.findElement(By.xpath("//input[@id='check-disbaled']"));
		WebElement slider02 = driver.findElement(By.xpath("//input[@id='slider-2']"));
		WebElement buttonIsDisabled = driver.findElement(By.xpath("//button[@id='button-disabled']"));

		isElementEnabled(passTextbox);
		isElementEnabled(ageRadioIsDisabled);
		isElementEnabled(jobRole2Select);
		isElementEnabled(interestsCheckboxIsDisabled);
		isElementEnabled(slider02);
		isElementEnabled(buttonIsDisabled);
		isElementEnabled(slider02);
	}	
	
	public void isElementEnabled(WebElement element) {
		if (element.isEnabled()) {
			System.out.println("Element is enabled");
		}
		else 
		{
			System.out.println("Element is disabled");
		}
	}
	
	@Test
	public void TC_03_VerifyElementIsClicked() {
		// TODO Auto-generated method stub
		driver.get("http://daominhdam.890m.com/");
		WebElement under18Radio = driver.findElement(By.xpath("//input[@id='under_18']"));
		WebElement interestsDevelopment = driver.findElement(By.xpath("//input[@id='development']"));
		under18Radio.click();
		interestsDevelopment.click();
		if (under18Radio.isSelected()) {
			System.out.println("Element under18Radio is selected ");
		}
		else
			under18Radio.click();
		
		if (interestsDevelopment.isSelected()) {
			System.out.println("Element interestsDevelopment is selected ");
		}
		else
			interestsDevelopment.click();
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
