package selenium_api;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;


import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.lang.model.element.Element;
import javax.swing.text.Document;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Topic_08_Exercise_JavascriptExecutor {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test(enabled = false)
	public void TC_01_JE() {
		driver.get("http://live.guru99.com/");
		String homePageDomain = (String) excuteForBrower("return document.domain");
		Assert.assertEquals("live.guru99.com", homePageDomain);
		String urlHomePage = (String) excuteForBrower("return document.URL");
		Assert.assertEquals("http://live.guru99.com/", urlHomePage);
		clickToElementByJS(findElement("//a[text()='Mobile']"));
		clickToElementByJS(findElement("//h2[a[@title  ='Samsung Galaxy']]/following-sibling:: div[@class='actions']/button[@title='Add to Cart']"));
		String textVerifySuccess = (String) excuteForBrower("return document.documentElement.innerText");
		Assert.assertTrue(textVerifySuccess.contains("Samsung Galaxy was added to your shopping cart."));
		clickToElementByJS(findElement("//a[text()='Privacy Policy']"));
		String getTitlePage = (String) excuteForBrower("return document.title");
		Assert.assertEquals("Privacy Policy", getTitlePage);
		scrollToBottomPage();
		Assert.assertTrue(findElement("//th[text()='WISHLIST_CNT']/following-sibling::td[text()='The number of items in your Wishlist.']").isDisplayed());
		excuteForBrower("window.location= 'http://demo.guru99.com/v4/'");
		String urlCurrentPage = (String) excuteForBrower("return document.URL");
		Assert.assertEquals("http://demo.guru99.com/v4/", urlCurrentPage);
	}

	@Test(enabled = false)
	public void TC_02_RemoveAttribute() {
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
		WebElement resultFrame = findElement("//iframe[@id='iframeResult']");
		driver.switchTo().frame(resultFrame);
		removeAttributeInDOM(findElement("//input[@name = 'lname']"), "disabled");
		String lastName = "Automation Testing";
		WebElement lastNameTextbox = findElement("//input[@name = 'lname']");
		senkeyToElementByJS(lastNameTextbox, lastName);
		clickToElementByJS(findElement("//input[@type='submit']"));
		String verifyText = findElement("//div[@class='w3-container w3-large w3-border']").getText();
		Assert.assertTrue(verifyText.contains(lastName));
	}

	@Test
	public void TC_03_CreateAccount() throws Exception {
		driver.get("http://live.guru99.com/");
		clickToElementByJS(findElement("//div[@class='footer']//a[@title='My Account']"));
		clickToElementByJS(findElement("//a[@title='Create an Account']"));
		String firstName, lastName, email, password, confirmPass;
		firstName = "Huyen";
		lastName = "Pham";
		email = "automation" + random() + "@gmail.com";
		password = "123456";
		confirmPass = "123456";
		senkeyToElementByJS(findElement(".//input[@id='firstname']"), firstName);
		senkeyToElementByJS(findElement(".//input[@id='lastname']"), lastName);
		senkeyToElementByJS(findElement(".//input[@id='email_address']"), email);
		senkeyToElementByJS(findElement(".//input[@id='password']"), password);
		senkeyToElementByJS(findElement(".//input[@id='confirmation']"), confirmPass);
		clickToElementByJS(findElement("//button[@title='Register']"));
		String textVerifySuccess = (String) excuteForBrower("return document.documentElement.innerText");
		Assert.assertTrue(textVerifySuccess.contains("Thank you for registering with Main Website Store."));
		clickToElementByJS(findElement("//div[@class='account-cart-wrapper']//span[text()='Account']"));
		clickToElementByJS(findElement("//div[@id='header-account']//a[@title='Log Out']"));
		Thread.sleep(7000);
		String titlePageCurrent = (String)excuteForBrower("return document.title");
		Assert.assertEquals("Home page", titlePageCurrent);
	}

	public WebElement findElement(String elementText) {
		return driver.findElement(By.xpath(elementText));
	}

	public Object excuteForBrower(String javascript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// trả về đoạn lệnh javascript. muốn lấy dữ liệu của đoạn lệnh thì phải return+
			// đoạnlệnh
			return js.executeScript(javascript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object clickToElementByJS(WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click()", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}

	}

	public Object scrollToBottomPage() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object removeAttributeInDOM(WebElement element, String attribute) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('" + attribute + "')", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object senkeyToElementByJS(WebElement element, String value) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].setAttribute('value', '" + value + "')", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public int random() {
		Random random = new Random();
		int number = random.nextInt(9999);
		return number;
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
