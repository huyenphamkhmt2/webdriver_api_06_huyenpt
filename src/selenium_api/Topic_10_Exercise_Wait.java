package selenium_api;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

@Test
public class Topic_10_Exercise_Wait {

	WebDriver driver;
	WebDriverWait waitExplicit;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		waitExplicit = new WebDriverWait(driver, 30);
	}

	public void TC_01_implicitWait() {
		driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		findElement("//div[@id='start']/button").click();
		Assert.assertTrue(findElement("//div[@id='finish']/h4[text()='Hello World!']").isDisplayed());
	}

	public void TC_02_explicitWait() {
		driver.get("http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		// check datetimepicker display
		Assert.assertTrue(findElement("//div[@id='ctl00_ContentPlaceholder1_RadCalendar1']").isDisplayed());
		WebElement selectedDate = findElement("//span[@id='ctl00_ContentPlaceholder1_Label1']");
		// check selected date = 'No Selected Dates to display.'
		Assert.assertEquals(selectedDate.getText(), "No Selected Dates to display.");
		// click today (6/11/2018)
		findElement("//a[text()='6']").click();

		// icon loading hien ra roi bien mat
		By ajaxIcon = By.xpath("//div[@class='raDiv']");
		waitExplicit.until(ExpectedConditions.invisibilityOfElementLocated(ajaxIcon));
		// verify seleted date
		Assert.assertTrue(findElement("//table[@id='ctl00_ContentPlaceholder1_RadCalendar1_Top']//a[text()='6']").isDisplayed());
		WebElement selectedDateAfter = findElement("//span[@id='ctl00_ContentPlaceholder1_Label1']");
		Assert.assertEquals(selectedDateAfter.getText(), "Tuesday, November 06, 2018");
	}

	public void TC_03_fluentWait() {
		driver.get("https://daominhdam.github.io/fluent-wait/");
		WebElement countdount = findElement("//div[@id='javascript_countdown_time']");
		waitExplicit.until(ExpectedConditions.visibilityOf(countdount));
		new FluentWait<WebElement>(countdount)
        // Tổng time wait là 15s
        .withTimeout(15, TimeUnit.SECONDS)
         // Tần số mỗi 1s check 1 lần
         .pollingEvery(1, TimeUnit.SECONDS)
        // Nếu gặp exception là find ko thấy element sẽ bỏ  qua
         .ignoring(NoSuchElementException.class)
         // Kiểm tra điều kiện
				.until(new Function<WebElement, Boolean>() {
					public Boolean apply(WebElement element) {
						// Kiểm tra điều kiện countdount = 00
						boolean flag = element.getText().endsWith("00");
						System.out.println("Time = " + element.getText());
						// return giá trị cho function apply
						return flag;
					}
				});
	}

	public WebElement findElement(String elementText) {
		return driver.findElement(By.xpath(elementText));
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
